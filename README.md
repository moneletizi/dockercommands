# Docker commands
![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Docker_%28container_engine%29_logo.svg/2560px-Docker_%28container_engine%29_logo.svg.png
)
### Goal
The main goal of this repo is to create a cheat sheet commands for docker.

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. 
Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.
-- <cite>from Docker FAQ</cite>
https://docs.docker.com/engine/faq/#what-does-docker-technology-add-to-just-plain-lxc

The main subjects covered from this guide are:
- [Definitions of the main components of Docker](#terminology)
- [Why containerization?](#container-vs-virtual-machine)
- [Container commands](#container-commands)
- [Image commands](#image-commands)
- [Compose some application and run relative containers by a single file](#compose)
- [Practice and exercises from the web](#practice)

## Terminology
| Name | Description | 
|-----------------|:-------------|
| **Image** | A static snapshot of container's configuration  | 
| **Container**     | An application sandbox. Each container is based on an image          | 
| **Docker registry**      | Remote places to store docker images (Docker hub is the most common, but provides only public images)         | 
| **Dockerfile** | A file that contains instructions to build docker images  | 
| **Docker engine**     | Docker platform installed and running on a host          | 
| **Docker client**      | Docker clients that talk with local or remote docker daemon         | 
| **Docker daemon**      | Service that listens to docker client commands       | 
| **Docker host**      | Server that runs Docker engine         | 
| **Volume**      | Directory shared between host and container         | 

![Simplified Docker architecture](https://geekflare.com/wp-content/uploads/2019/09/docker-architecture-609x270.png)
*Image taken from GeekFlare* https://geekflare.com/docker-architecture/
## Container VS Image

## Container VS Virtual Machine
Now in Docker container’s case, you have a single OS, and the resources are shared between the containers. Hence, it is lightweight and boots in seconds.

## Container commands
- **RUN** a container specifying the relative image
```
docker run -d -p <HOST_PORT>:<CONTAINER_PORT> --network=<NETWORK_NAME> --name=<CONTAINER-NAME> <IMAGE_NAME>
```

Others params can be passed to docker run command.
1. `-w <DIRECTORY_NAME>` to set working directory
2. `-v <HOST_SRC>:<CONTAINER_DEST>` to mount a volume between host and container file system
3. `--read-only` to mount the container's root file system as read only
4. `-e NAME=VALUE` to set environment variable
5. `-d` to run in detached mode (to run the container and return to the terminal to write the next command)

- **VISUALIZE** all containers running
```
docker ps -a 
```
To visualize only running containers
```
docker ps
```
To visualize a container with a specific name
```
docker ps -a | grep <CONTAINER_NAME>
```
- **DELETE** a container
```
docker rm <CONTAINER_NAME | CONTAINER_ID>
```
- **OPEN A BASH** terminal inside a container
```
docker exec -it <CONTAINER_ID> /bin/bash
```
If bash doesn't work, try open a shell
```
docker exec -it <CONTAINER_ID> /bin/sh
```
- See the **LOGS** relates to a container
```
docker logs <CONTAINER_ID>
```

## Image commands
- **DELETE** an images
```
docker rmi <IMAGE_TAG | IMAGE_ID>
```
- **VISUALIZE** all images
```
docker images
```

- **BUILD** an image from DockerFile
```
docker build -t <IMAGENAME>:<IMAGEVERSION> .
```
The last parameter is the folder where the Dockerfile is situated; if you run the command from the same folder . indicates the current folder, otherwise, select your own path (./<SOME_PATH>)

## Compose  
Composing Images and run the associated containers inside a network is a common practice to work easily when you have some different system that wants to communicates.

- Create containers based on docker compose file and starts them. By default create a network in the host.
```
docker-compose -f <DOCKER_COMPOSE_FILE> -up
```
## Volumes
We need Docker volumes when an application is stateful and need persistence.<br>
A container runs on a host and the container has its own virtual file system; if we remove, stop, restart the container, all the data is gone. <br>
On a host we have a physical file system; basically, a folder in the physical host file system is mounted in the virtual file system of Docker.
When a container write on its own file system the data were replicated in the host file system and vice versa.

3 Docker volumes type:
- HOST VOLUME : `-v <HOST_DIR>:<CNTR_DIR>` example: `-v /home/data:/var/lib/mysql/data`
- ANONYMOUS VOLUME : You don't specify any directory from the host; it is automatically created by Docker `-v <CNTR_DIR>` example: `-v /var/lib/mysql/data`
- NAMED VOLUME : Specify only the name of the folder on the host `-v name:<CNTR_DIR>` example: `-v folder:/var/lib/mysql/data`. Is the most used

## Practice
Some sites to do exercises and practice:
- https://dockerlabs.collabnix.com

## Acknowledgement:
- TechWorld with Nana https://www.youtube.com/watch?v=3c-iBn73dDE
- 